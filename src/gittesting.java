public class gittesting {
/**
    * Given a string, compute recursively (no loops) the number
    of "11"
    * substring in the string. The "11" substrings should not
    overlap.
    * count11("11abc11") 3
    * count11("111") true
    * nestParen("((()))") false
    */
    public static boolean nestParen(String str) {
        return false;
        
    }
    public static boolean nestParen_2(String str) {
        return false;
    }
    
    public static void main(String[] args) {
        System.out.println(count11("11abc11"));
        System.out.println(count11("abc11x11x11"));
        System.out.println(count11("111"));
        System.out.println("-".repeat(10));
        System.out.println(count11_2("11abc11"));
        System.out.println(count11_2("abc11x11x11"));
        System.out.println(count11_2("111"));
        System.out.println("-".repeat(10));
        System.out.println(nestParen("(())"));
        System.out.println(nestParen("((()))"));
        System.out.println(nestParen("(((x))"));
        System.out.println("-".repeat(10));
        System.out.println(nestParen_2("(())"));
        System.out.println(nestParen_2("((()))"));
        System.out.println(nestParen_2("(((x))"));
    }
    
    public static int count11(String str) {
        if (str.length() < 2) {
            return 0;
        }
        
        if (str.substring(0,2).equals("11")) {
            return 1 + count11(str.substring(2));
        } else {
            return count11(str.substring(1));
        }
    }
        
    public static int count11_2(String str) {
        return count11Helper(str, 0);
    }
    
    public static int count11Helper(String str, int i) {
        if (i >= str.length() - 1) {
            return 0;
        }
        
        if (str.charAt(i) == '1' && str.charAt(i + 1) == '1') {
            return 1 + count11Helper(str, i + 2);
           
        } else {
            return count11Helper(str, i + 1);
        }
    }
}
